/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g1;

import java.util.Date;

/**
 *
 * @author 1181539
 */
public class Condutor {
    private int CC; 
    private double qtdAlcool; 
    private Date dataNascimento;
    private char genero;
    
    public Condutor(int CC, double qtdAlcool, Date dataNascimento, char genero) {
        this.CC = CC;
        this.qtdAlcool = qtdAlcool;
        this.dataNascimento = dataNascimento;
        this.genero = genero;
    }
    
    public void setCC(int CC) {
        this.CC = CC;
    }
    
    public int getCC() {
        return this.CC;
    }

    public void setQtdAlcool(double qtdAlcool) {
        this.qtdAlcool = qtdAlcool;
    }  
    
    public double getQtdAlcool() {
        return this.qtdAlcool;
    }
    
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    
    public Date getDataNascimento() {
        return this.dataNascimento;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }
    
    public char getGenero() {
        return this.genero;
    }
}
