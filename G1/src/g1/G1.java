/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Goldb
 */
public class G1 {
    
    public static Scanner scan = new Scanner (System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
        int numCC;
        double quantAlcool;
        Date dataNascimento;
        char genero;
        Condutor[] condutor = new Condutor[99];
        int count = 0;
        do {
            System.out.println("Introduza o número do bilhete de identidade");
            numCC = scan.nextInt();
            
            if (numCC == 0 ){
                break;
            }
            System.out.println("Introduza o género do paciente/a");
            genero = scan.next().charAt(0);
            System.out.println("Introduza a data de nascimento do paciente/a");
            dataNascimento = format.parse(scan.next());
            System.out.println("Introduza a quantidade de álcool no sangue do paciente/a");
            quantAlcool = scan.nextDouble();
            
            condutor[count] = new Condutor(numCC, quantAlcool, dataNascimento, genero);
            count++;
        } while(numCC != 0 );
            
        int countMenor30 = 0;
        int countM = 0;
        int countHMaior05 = 0;
        Date trintaAnos = format.parse("01-01-1988");
        for (int i = 0; i < 99; i++) {
            if ((condutor[i] != null) && (condutor[i].getDataNascimento().before(trintaAnos))) {
                countMenor30++;
            }
            
            if ((condutor[i] != null) && (condutor[i].getGenero() == 'F')) {
                countM++;
            }
            
            if ((condutor[i] != null) && (condutor[i].getGenero() == 'M') && (condutor[i].getQtdAlcool() > 0.5)) {
                countHMaior05++;
            }
        }
        
        System.out.println("A percentagem de condutores com menos de 30 anos é: " + ((countMenor30 * 100) / count));
        System.out.println("A percentagem de condutores femininas é: " + ((countM * 100) / count));
        System.out.println("A percentagem de condutores masculinos com mais de 0.5 é: " + ((countHMaior05 * 100) / count));
    }
    
}
